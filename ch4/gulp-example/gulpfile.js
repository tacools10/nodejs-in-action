const gulp = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const babel = require('gulp-babel');
const concat = require('gulp-concat');
const path = './app/*.jsx';

gulp.task('default', () => {
    return gulp.src(path)
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['@babel/preset-env', '@babel/preset-react']
        }))
        .pipe(concat('all.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist'));
});

gulp.task('watch', () => {
    gulp.watch(path, gulp.series('default'));
});

