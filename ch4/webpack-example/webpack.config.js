const path = require('path');
const webpack = require('webpack');

module.exports = {
    entry: './app/index.js',
    output: { 
        path: __dirname, 
        filename: 'bundle.js',
        publicPath: '/assets/'
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 3001,
        hot: true,
        inline: true
    },
    module: {
        rules: [
            {
                test: /.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                options: {
                    presets: ['@babel/preset-react', '@babel/preset-env']
                }
            }
        ]
    }
};
