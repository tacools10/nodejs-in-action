const fs = require('fs');
const w = require('./watcher');
const path = require('path');

const watchPath = process.cwd() + '/watchDirectory';
const processedPath = process.cwd() + '/processedDirectory';

const content = 'This is the contents of my FILE';

const createDir = (dirPath) => {
    fs.mkdirSync(process.cwd() + dirPath, {recursive : true}, (error) => {
        if (error) {
            console.error('An error occured', error);
        } else {
            console.log('Your directory is made');
        }
    });
};

const createFile = (filePath, fileContent) => {
    fs.writeFile(filePath, fileContent, (error) => {
        if (error) {
            console.error('An error occured: ', error);
        } else {
            console.log(`Your file is made at ${filePath}`);
        }
    });
    console.log(path.resolve(filePath));
};

const watcher = new w(path.resolve(watchPath), path.resolve(processedPath));

watcher.on('process', (file) => {
    console.log("New process event emitted!");
    const watchFile = `${watcher.watchDir}/${file}`;
    const processedFile = `${watcher.processedDir}/${file.toLowerCase()}`;
    fs.rename(watchFile, processedFile, err => {
            if (err) throw err;
            else console.log("rename successful");
    });
});

createDir(watchPath);
createDir(processedPath);
createFile(process.cwd() + watchPath + '/content.txt', content);
watcher.start(); 





